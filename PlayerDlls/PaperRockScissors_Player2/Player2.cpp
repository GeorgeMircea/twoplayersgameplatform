#include "Player2.h"


IPlayer * Player2::getInstance()
{
	if (instance == nullptr)
		instance = new Player2();
	return instance;
}

Player2::Player2(std::string name) :m_name(name)
{
}

std::string Player2::getMessage()
{
	++index;
	index %= 3;
	switch (index)
	{
	case 0:
		return"Paper";
	case 1:
		return "Rock";
	case 2:
		return "Scissors";
	default:
		return "";
	}

}

void Player2::setMessage(std::string)
{
}

void Player2::setName(std::string name)
{
	m_name = name;
}

std::string Player2::getName()
{
	return m_name;
}
