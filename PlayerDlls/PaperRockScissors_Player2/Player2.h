#pragma once
#include"..\Headers.h"
class Player2 :
	public IPlayer
{
	std::string m_name;
	int index = 0;
	
	static IPlayer* instance;
	Player2() = default;

public:
	static IPlayer* getInstance();
	Player2(std::string name);
	virtual ~Player2() = default;

	// Inherited via IPlayer
	virtual std::string getMessage() override;
	virtual void setMessage(std::string) override;
	virtual void setName(std::string) override;
	virtual std::string getName() override;
};

extern "C"
{
	__declspec(dllexport) IPlayer* getPlayer();
};


//create a PaperRockScissors instance
IPlayer* getPlayer()
{
	return Player2::getInstance();
}