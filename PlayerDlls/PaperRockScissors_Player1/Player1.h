#pragma once
#include"..\Headers.h"
#include<random>

class Player1 :
	public IPlayer
{
	std::string m_name;

	Player1() = default;

public:
	static IPlayer* getInstance();
	Player1(std::string name);
	virtual ~Player1() = default;

	// Inherited via IPlayer
	virtual std::string getMessage() override;
	virtual void setMessage(std::string message) override;
	virtual void setName(std::string) override;
	virtual std::string getName() override;
};

extern "C"
{
	__declspec(dllexport) IPlayer* getPlayer();
};


//create a PaperRockScissors instance
IPlayer* getPlayer()
{
	return Player1::getInstance();
}
