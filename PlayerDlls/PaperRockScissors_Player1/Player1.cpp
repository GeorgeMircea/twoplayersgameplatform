#include "Player1.h"


IPlayer * Player1::getInstance()
{
	if (instance == nullptr)
		instance = new Player1();
	return instance;
}

Player1::Player1(std::string name) :m_name(name)
{
}

std::string Player1::getMessage()
{
	const auto NUMBER_OF_POSSIBLE_CHOICES = 3;
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, NUMBER_OF_POSSIBLE_CHOICES - 1);

	int choice = dis(gen);
	switch (choice)
	{
	case 0:
		return"Paper";
	case 1:
		return "Rock";
	case 2:
		return "Scissors";
	default:
		return "";
	}
}

void Player1::setMessage(std::string message)
{
}

void Player1::setName(std::string name)
{
	m_name = name;
}

std::string Player1::getName()
{
	return m_name;
}
