#pragma once
#include<string>

class IPlayer
{
public:
	IPlayer() = default;
	virtual ~IPlayer() = default;

	//returns the move made by the player
	//the move is considered a string
	virtual std::string getMessage() = 0;
	
	//sets the message receved by the player from the game 
	virtual void setMessage(std::string) = 0;

	//sets the name of the player
	virtual void setName(std::string) = 0;

	//gets the name of the player
	virtual std::string getName() = 0;
};
