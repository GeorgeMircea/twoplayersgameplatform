#pragma once
#include<string>
#include<array>
#include"IPlayer.h"
const int MAX_NUMBER_OF_PLAYERS = 50;
typedef std::array<std::string, MAX_NUMBER_OF_PLAYERS> ArrayOfNPlayers;
class ILoader
{
public:
	ILoader() = default;
	virtual ~ILoader() = default;

	//returns a list of players which can connect to the server
	//or can be loaded to the server
	virtual ArrayOfNPlayers  findAllPlayers(std::string playersFolder) = 0;

	//load a specific player and returns it
	virtual IPlayer* loadPlayer(std::string playerName) = 0;

	//returns  the actual number of players
	virtual int getNumberOfPlayers() = 0;

	//send message to playerName player
	virtual void sendMessageToPlayer(std::string message, std::string playerName) = 0;

	//receive message from playerName player
	virtual std::string receiveMessageFromPlayer(std::string playerName) = 0;
};