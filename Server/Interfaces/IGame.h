#pragma once
#include<string>
#include<array>
#include<sstream>

const int NUMBER_OF_PLAYERS = 2;
enum class Winner
{
	PLAYER_ONE,
	PLAYER_TWO,
	DRAW,
	UNDISPUTED
};

class IGame
{
public:
	IGame() = default;
	virtual ~IGame() = default;

	//decides which player wins or if it's a draw
	virtual void decideWinner() = 0;

	//returns an element from winner enum
	virtual Winner getWinner() const = 0;

	//returns the name of the game
	virtual std::string getGameName() = 0;

	//returns true if the game is ended
	virtual bool gameEnded() = 0;

	//gets the message
	virtual std::string getMessage() = 0;

	//sets the message
	virtual void setMessage(std::string) = 0;

	//checks if the game has a board
	virtual bool hasBoard() = 0;
};

