#pragma once
#include<Windows.h>
#include<memory>
#include<iostream>
#include<random>

#include"..\Interfaces\IGame.h"
#include"..\Interfaces\IPlayer.h"
#include"..\Interfaces\ILoader.h"


//this class is sloads the dll which represents the played game
//and dlls which represent the players
class Server
{

	IGame* m_playedGame;
	HINSTANCE dllHandle;

	IPlayer* m_player1;
	IPlayer* m_player2;

	ILoader* m_loader;

	//the list of all players
	ArrayOfNPlayers m_playersList;

	int m_numberOfPlayers = 0;

	//select 2 random players from the playersList
	void selectPlayers();

	//loads a single player
	IPlayer* loadPlayer(std::string playerName);

	//displays the winner
	void displayWinner();
public:
	Server() = default;
	~Server() = default;

	//load the chosen loader
	void setLoader(std::string loaderName);

	//loads the game at the gamePath location
	//the game has to implement IGame interface ("..\Interfaces\IGame.h")
	void loadGame(std::string gamePath);

	//finds all players from playersFolder into playersList member
	//every player has to implement IPlayer interface ("..\Interfaces\IPlayer.h")
	void findAllPlayers(std::string playersFolder = "Players\\");

	//starts the chosen game with selected players
	void startGame();

};

