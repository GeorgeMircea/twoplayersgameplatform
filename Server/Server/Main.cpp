#include"Server.h"
#include<sstream>

int main()
{
	std::unique_ptr<Server> server=std::make_unique<Server>();
	server->setLoader("DllLoader");
	server->loadGame("PaperRockScissors");
	server->findAllPlayers();

	server->startGame();

	/**********************************************************************************/

	return 0;
}