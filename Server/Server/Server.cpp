#include "Server.h"

void Server::selectPlayers()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, m_numberOfPlayers - 1);

	//choose 2 random different players
	int firstChoice = dis(gen);
	int secondChoice;
	do
	{
		secondChoice = dis(gen);
	} while (firstChoice == secondChoice);

	m_player1 = loadPlayer(m_playersList[firstChoice]);
	m_player1->setName(m_playersList[firstChoice]);
	m_player1Name = m_playersList[firstChoice];

	m_player2 = loadPlayer(m_playersList[secondChoice]);
	m_player2->setName(m_playersList[secondChoice]);
	m_player2Name = m_playersList[secondChoice];
}

void Server::displayWinner()
{
	Winner winner = m_playedGame->getWinner();
	switch (winner)
	{
	case Winner::PLAYER_ONE:
		std::cout << m_player1->getName() << " WON!" << std::endl;
		return;
	case Winner::PLAYER_TWO:
		std::cout << m_player2->getName() << " WON!" << std::endl;
		return;
	case Winner::DRAW:
		std::cout << "DRAW!" << std::endl;
		return;
	default:
		std::cout << "Error at deciding the winner!";
		return;
	}
}

void Server::setLoader(std::string loaderName)
{
	//Load the dll and keep the handle to it
	typedef ILoader*(CALLBACK* FUNCTYPE)();
	FUNCTYPE  pLoader = NULL;

	loaderName += ".dll";

	std::string loaderPath = loaderName;

	TCHAR* gameNameTCHAR = new TCHAR[loaderPath.length() + 1];
	std::copy(loaderPath.begin(), loaderPath.end(), gameNameTCHAR);
	gameNameTCHAR[loaderPath.length()] = '\0';
	dllHandle = LoadLibrary(gameNameTCHAR);

	// If the handle is valid, try to get the function address. 
	if (dllHandle != nullptr)
	{
		//Get pointer to our function using GetProcAddress:
		pLoader = (FUNCTYPE)GetProcAddress(dllHandle, "getLoader");

		// If the function address is valid, call the function. 
		if (pLoader != nullptr)
		{
			m_loader = pLoader();
		}
		else
		{
			std::cout << "getLoader() method doesn't exist in your dll!" << std::endl;
		}
	}
	else
	{
		std::cout << "Invalid dll!" << std::endl;
	}

}

void Server::loadGame(std::string gameName)
{
	//Load the dll and keep the handle to it
	typedef IGame*(CALLBACK* FUNCTYPE)();
	FUNCTYPE  pGame = NULL;

	gameName += ".dll";

	std::string gamePath = "Games\\" + gameName;

	TCHAR* gameNameTCHAR = new TCHAR[gamePath.length() + 1];
	std::copy(gamePath.begin(), gamePath.end(), gameNameTCHAR);
	gameNameTCHAR[gamePath.length()] = '\0';
	dllHandle = LoadLibrary(gameNameTCHAR);

	// If the handle is valid, try to get the function address. 
	if (dllHandle != nullptr)
	{
		//Get pointer to our function using GetProcAddress:
		pGame = (FUNCTYPE)GetProcAddress(dllHandle, "getGame");

		// If the function address is valid, call the function. 
		if (pGame != nullptr)
		{
			m_playedGame = pGame();
		}
		else
		{
			std::cout << "getGame() method doesn't exist in your dll!" << std::endl;
		}

		std::cout << m_playedGame->getGameName() << " loaded!" << std::endl;
	}
	else
	{
		std::cout << "Invalid dll!" << std::endl;
	}

}

void Server::findAllPlayers(std::string playersFolder)
{
	m_playersList = m_loader->findAllPlayers(playersFolder);
	m_numberOfPlayers = m_loader->getNumberOfPlayers();
}

void Server::startGame()
{
	//select 2 players
	selectPlayers();

	//while there is no winner
	while (!m_playedGame->gameEnded())
	{

		//server receives a message from the first player
		auto messageFromPlayer = m_loader->receiveMessageFromPlayer(m_player1Name);

		//server sends the message to the game
		m_playedGame->setMessage(messageFromPlayer + " Player1");

		//server receives a message from the game
		auto messageFromGame = m_playedGame->getMessage();

		//server sends the message to both players
		m_loader->sendMessageToPlayer(messageFromGame, m_player1Name);
		m_loader->sendMessageToPlayer(messageFromGame, m_player2Name);

		//check if someone won
		if (!m_playedGame->gameEnded())
		{
			//server receives a message from the second player
			messageFromPlayer = m_loader->receiveMessageFromPlayer(m_player2Name);

			//server sends the message to the game
			m_playedGame->setMessage(messageFromPlayer + " Player2");

			//server receives a message from the game
			messageFromGame = m_playedGame->getMessage();

			//server sends the message to both players
			m_loader->sendMessageToPlayer(messageFromGame, m_player1Name);
			m_loader->sendMessageToPlayer(messageFromGame, m_player2Name);
		}
	}
	displayWinner();
}

IPlayer * Server::loadPlayer(std::string playerName)
{
	return m_loader->loadPlayer(playerName);
}
