#pragma once
#include"..\Headers.h"

class PaperRockScissors :
	public IGame
{
	enum class Move
	{
		PAPER,
		ROCK,
		SCISSORS,
		UNKNOWN
	};


	Winner m_winner = Winner::UNDISPUTED;

	Move m_firstPlayerMove = Move::UNKNOWN;
	Move m_secondPlayerMove = Move::UNKNOWN;



	bool firstPlayerWins() const;
	bool secondPlayerWins() const;
	bool isDraw() const;

	//convert string to move
	std::string moveConverter(Move move);

	//convert move to string
	Move moveConverter(std::string move);

	//intreprets the message setting the moves of both players
	void interpretMessage(std::string message);

	PaperRockScissors() = default;
	
public:
	virtual ~PaperRockScissors() = default;

	static IGame& getInstance();
	// Inherited via IGame
	virtual void decideWinner() override;
	virtual Winner getWinner() const override;
	virtual std::string getGameName() override;
	virtual bool gameEnded() override;
	virtual bool hasBoard() override;

	//the message contains the move and the player who played it
	//delimited by space
	virtual void setMessage(std::string message) override;

	//the message contains the moves of both players
	//or an empty string
	//the actual message is returned only if both players made their move
	virtual std::string getMessage() override;
};

extern "C"
{
	__declspec(dllexport) IGame* getGame();
};


//create a PaperRockScissors instance
IGame* getGame()
{
	return &PaperRockScissors::getInstance();
}
