#pragma once
#include"..\Headers.h"
class TicTacToe :
	public IGame
{

	TicTacToe() = default;
public:
	static IGame& getInstance();
	virtual ~TicTacToe() = default;

	// Inherited via IGame
	virtual void decideWinner() override;
	virtual Winner getWinner() const override;
	virtual std::string getGameName() override;
	virtual bool gameEnded() override;
	virtual bool hasBoard() override;

	//
	virtual std::string getMessage() override;

	//
	virtual void setMessage(std::string) override;

};

extern "C"
{
	__declspec(dllexport) IGame* getGame();
};


//create a TicTacToe instance
IGame* getGame()
{
	return &TicTacToe::getInstance();
}
