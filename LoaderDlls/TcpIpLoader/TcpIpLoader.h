#pragma once
#include"..\Headers.h"
class TcpIpLoader :
	public ILoader
{

	int m_numberOfPlayers = 0;
	TcpIpLoader() = default;
public:
	static ILoader& getInstance();
	virtual ~TcpIpLoader() = default;

	// Inherited via ILoader
	virtual ArrayOfNPlayers findAllPlayers(std::string playersFolder) override;
	virtual IPlayer * loadPlayer(std::string playerName) override;
	virtual int getNumberOfPlayers();

	// Inherited via ILoader
	virtual void sendMessageToPlayer(std::string message, std::string playerName) override;
	virtual std::string receiveMessageFromPlayer(std::string playerName) override;
};


extern "C"
{
	__declspec(dllexport) ILoader* getLoader();
};


//create a PaperRockScissors instance
ILoader* getLoader()
{
	return &TcpIpLoader::getInstance();
}
