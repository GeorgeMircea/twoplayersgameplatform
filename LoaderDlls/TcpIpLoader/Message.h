#pragma once
#include <string>
#define MAX_PACKET_SIZE 1000000
class Message
{
public:
	Message() = default;
	~Message() = default;

	std::string m_message;

	void serialize(char * data);
	void deserialize(char * data);
};

