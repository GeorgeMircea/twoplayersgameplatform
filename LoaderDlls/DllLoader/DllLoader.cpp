#include "DllLoader.h"



ILoader & DllLoader::getInstance()
{
	static DllLoader instance;
	return instance;
}

ArrayOfNPlayers DllLoader::findAllPlayers(std::string playersFolder)
{
	ArrayOfNPlayers playersList;

	WIN32_FIND_DATA FindFileData;
	std::string img = playersFolder + "PaperRockScissors_*.dll";

	TCHAR* file = new TCHAR[img.length() + 1];
	std::copy(img.begin(), img.end(), file);
	file[img.length()] = '\0';

	HANDLE hFind = FindFirstFile(file, &FindFileData);


	if (hFind == INVALID_HANDLE_VALUE)
	{
		return ArrayOfNPlayers();
	}
	else
	{
		do
		{
			std::wstring fileName(FindFileData.cFileName);
			playersList[m_numberOfPlayers++] = std::string(fileName.begin(), fileName.end());
			//std::cout << m_playersList[m_playerIndex - 1] << std::endl;
		} while (FindNextFile(hFind, &FindFileData));

	}
	FindClose(hFind);
	return playersList;
}

IPlayer * DllLoader::loadPlayer(std::string playerName)
{
	typedef IPlayer*(CALLBACK* FUNCTYPE)();
	FUNCTYPE  player = NULL;

	std::string playerPath = "Players\\" + playerName;

	TCHAR* gameNameTCHAR = new TCHAR[playerPath.length() + 1];
	std::copy(playerPath.begin(), playerPath.end(), gameNameTCHAR);
	gameNameTCHAR[playerPath.length()] = '\0';
	HINSTANCE dllHandle = LoadLibrary(gameNameTCHAR);

	// If the handle is valid, try to get the function address. 
	if (dllHandle != nullptr)
	{
		//Get pointer to our function using GetProcAddress:
		player = (FUNCTYPE)GetProcAddress(dllHandle, "getPlayer");

		// If the function address is valid, call the function. 
		if (player != nullptr)
		{
			if (playerCount == 0)
			{
				m_player1 = player();
				m_player1->setName(playerName);
			}
			else
			{
				m_player2 = player();
				m_player2->setName(playerName);
			}
			playerCount = 1 - playerCount;
			return player();
		}
		else
		{
			return nullptr;
		}

	}
	else
	{
		return nullptr;
	}

}

int DllLoader::getNumberOfPlayers()
{
	return m_numberOfPlayers;
}

void DllLoader::sendMessageToPlayer(std::string message, std::string playerName)
{
	if (playerName == m_player1->getName())
		m_player1->setMessage(message);
	if (playerName == m_player2->getName())
		m_player2->setMessage(message);
}

std::string DllLoader::receiveMessageFromPlayer(std::string playerName)
{
	if(playerName==m_player1->getName())
		return m_player1->getMessage();
	if (playerName == m_player2->getName())
		return m_player2->getMessage();
	return "";
}
