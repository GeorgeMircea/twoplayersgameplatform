#pragma once
#include"..\Headers.h"
#include<Windows.h>
#include<array>


class DllLoader :
	public ILoader
{
	DllLoader() = default;
	int m_numberOfPlayers = 0;


	IPlayer* m_player1;
	IPlayer* m_player2;

public:
	static ILoader & getInstance();
	virtual ~DllLoader() = default;

	//returns a string formed by all players path from playersFolder
	virtual ArrayOfNPlayers findAllPlayers(std::string playersFolder) override;

	//load a specific player
	virtual IPlayer * loadPlayer(std::string playerName) override;

	virtual int getNumberOfPlayers() override;

	//sets message to playerName player
	virtual void sendMessageToPlayer(std::string message, std::string playerName) override;
	
	//returns message from playerName
	virtual std::string receiveMessageFromPlayer(std::string playerName) override;

};


extern "C"
{
	__declspec(dllexport) ILoader* getLoader();
};


//create a PaperRockScissors instance
ILoader* getLoader()
{
	return &DllLoader::getInstance();
}
